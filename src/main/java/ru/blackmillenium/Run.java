package ru.blackmillenium;

import org.apache.log4j.Logger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import ru.blackmillenium.job.InterruptJob;

import static org.quartz.JobBuilder.newJob;

public class Run {
    private static final Logger LOGGER = Logger.getLogger(Run.class);

    public static void main(String[] args) {
        Scheduler scheduler = SchedulerSingleton.getInstance();

        JobDetail jobDetail = newJob(InterruptJob.class)
                .withIdentity("job")
                .storeDurably()
                .build();

        try {
            scheduler.addJob(jobDetail, false);
            scheduler.triggerJob(jobDetail.getKey());

            LOGGER.info("Усыпляем основной поток на 10 секунд");
            Thread.sleep(10000);

            LOGGER.info("Перываем задачу");
            scheduler.interrupt(jobDetail.getKey());
        } catch (SchedulerException | InterruptedException e) {
            e.printStackTrace();
        }

    }
}
