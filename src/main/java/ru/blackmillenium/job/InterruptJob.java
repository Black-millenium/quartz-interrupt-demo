package ru.blackmillenium.job;

import org.apache.log4j.Logger;
import org.quartz.*;

import java.util.Date;

public class InterruptJob implements InterruptableJob {
    private static final Logger LOGGER = Logger.getLogger(InterruptJob.class);

    private final Object waitLock = new Object();
    private volatile boolean canInterrupted = true;
    private JobKey jobKey = null;
    private volatile Thread thisThread;

    @Override
    public void interrupt() throws UnableToInterruptJobException {
        LOGGER.info("Задача " + jobKey + "  -- INTERRUPTING --");

        if (thisThread != null) {

            synchronized (waitLock) {
                if (!canInterrupted) try {
                    LOGGER.info("Ждем пока завершится длительная задача");
                    waitLock.wait();
                } catch (InterruptedException e) {
                    LOGGER.error("Ошибка ожидания потока");
                }
            }

            thisThread.interrupt();
        }
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        thisThread = Thread.currentThread();
        LOGGER.info("Текущий тред: " + thisThread.getName());

        jobKey = context.getJobDetail().getKey();
        LOGGER.info("Задача " + jobKey + " начала выполнение " + new Date());

        try {
            LOGGER.info("Начинаем долгую задачу");

            canInterrupted = false;
            Thread.sleep(30 * 1000); // тут какая-то длительная операция ввода-вывода или еще чего-нибудь
            canInterrupted = true;

            synchronized (waitLock) {
                LOGGER.info("Уведомляем ожидающий поток, что тут свободно");
                waitLock.notify();
            }

            LOGGER.info("Долгая задача завершена");
            LOGGER.info("Тут просто чт-то делаем, что можно прервать");
            Thread.sleep(300 * 1000);
            LOGGER.info("Прервано");
        } catch (InterruptedException e) {
            LOGGER.info("Поток остановлен.");
            try {
                context.getScheduler().shutdown();
            } catch (SchedulerException e1) {
                e1.printStackTrace();
            }
            throw new JobExecutionException("Поток остановлен", e);
        }
    }
}
